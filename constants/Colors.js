const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  headerColor:'black', //color of header 'green color'
  SelectedCategoryColor:'#97c689',//color of selected category  page
  SelectedSubCategoryColor:'#bfdcb6',//color of selected  back ground of sub category page
  unSelectedCategoryColor:'#ece7e3',//color of Unselected category  page
};
