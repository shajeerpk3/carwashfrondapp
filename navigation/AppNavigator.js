import React from 'react';
import { createAppContainer, createSwitchNavigator,StackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import Login from '../screens/LoginandRegister/Login';
import MainRegister from '../screens/LoginandRegister/MainRegister';
import MainLogin from '../screens/LoginandRegister/MainLogin';
import AddVehicle from '../screens/Addvehicle/Addvehicle';
import WebviewForCondtions from '../screens/AccountDetails/WebVidew';
import AccountUpdate from '../screens/AccountDetails/UpdateAccount';
import Qrcode from '../screens/QrCodeScanner/Qrcode';
import AddPackages from '../screens/addPackages/AddPackageMain';
import Payment from '../screens/Payment/PaymentMain';
import Paypal from '../screens/Payment/Paypal';
import Washing from '../screens/Wash/WashMain'
export default createAppContainer(createSwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  Main: MainTabNavigator,
  Login:Login,
  MainLoginRegister:MainRegister,
  MainLogin:MainLogin,
  AddPackages:AddPackages,
  WebviewForCondtions:WebviewForCondtions,
  AccountUpdate:AccountUpdate,
  Qrcode:Qrcode,
  AddVehicle:AddVehicle,
  Payment:Payment,
  Paypal:Paypal,
  Washing:Washing
},{
  initialRouteName : 'Main'
}));