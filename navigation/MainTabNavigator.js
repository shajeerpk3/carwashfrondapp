import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import CommonColor from '../constants/Colors';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/Home/HomeMain';

import Qrcode from '../screens/QrCodeScanner/Qrcode'
import AccountScreen from '../screens/AccountDetails/Account'

import { createIconSetFromIcoMoon,createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../src/config.json';
import icoMoonConfig from '../src/selection.json';

const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const Icon = createIconSetFromFontello(fontelloConfig);


const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'HOME',


  tabBarOptions: {
    // hide labels
    activeTintColor: CommonColor.headerColor, // active icon color

    // inactiveTintColor: '#586589',  // inactive icon color
    labelStyle: {
      bottom: 5,
      marginBottom: 5, fontSize: 17, zIndex: 5, fontWeight: 'bold'
    },
    style: {
      borderTopWidth: 5,// TabBar background
      borderTopColor: CommonColor.headerColor,
      height: 70
    }
  },
  tabBarIcon: ({ tintColor }) => (


    <Icon2 name="home" size={30} color={tintColor} />
  ),
};


const CartStack = createStackNavigator({
  Cart: Qrcode,
});

CartStack.navigationOptions = {
  tabBarLabel: 'SCAN QR',


  tabBarOptions: {
    // hide labels
    activeTintColor: CommonColor.headerColor, // active icon color

    // inactiveTintColor: '#586589',  // inactive icon color
    labelStyle: {
      bottom: 5,
      marginBottom: 5, fontSize: 17, zIndex: 5, fontWeight: 'bold'
    },
    style: {
      borderTopWidth: 5,// TabBar background
      borderTopColor: CommonColor.headerColor,
      height: 70
    }
  },
  tabBarIcon: ({ tintColor }) => (


    <Icon2 name="qrcode" size={26} color={tintColor} />
  ),
};


const AccountStack = createStackNavigator({
  Account: AccountScreen,
});

AccountStack.navigationOptions = {
  tabBarLabel: 'ACCOUNTS',


  tabBarOptions: {
    // hide labels
    activeTintColor: CommonColor.headerColor, // active icon color

    // inactiveTintColor: '#586589',  // inactive icon color
    labelStyle: {
      bottom: 5,
      marginBottom: 5, fontSize: 17, zIndex: 5, fontWeight: 'bold'
    },
    style: {
      borderTopWidth: 5,// TabBar background
      borderTopColor: CommonColor.headerColor,
      height: 70
    }
  },
  tabBarIcon: ({ tintColor }) => (


    <Icon2 name="Account" size={28} color={tintColor} />
  ),
};



export default createBottomTabNavigator({
  HomeStack,
  CartStack,
  AccountStack,
});
