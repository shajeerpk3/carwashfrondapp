import React from "react";
import { Linking, ScrollView, Text, TouchableOpacity, StyleSheet, View, Dimensions } from "react-native";
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../src/selection.json';
import Settings from "../../constants/Settings";
import Header from '../MainScreens/Header';
import Layout from '../../constants/Layout';
// import GluxHome from "../GluxHome/Home";
// import SearchHome from "../GluxHome/search";
import Api4 from "../../utils/Api/GetPayHistory"
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import { NavigationEvents } from "react-navigation";
import Communications from 'react-native-communications';
import currencyConvertor from '../../constants/currency_DateFormate'
import Colors from '../../constants/Colors';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

const windowhalfwidthSecond = Dimensions.get('window').width / 3;
export default class Accounts extends React.Component {
  static navigationOptions = ({ title, subtitle }) => {
    return {
      headerStyle: {
        height: Layout.headerHeight,
        width: Layout.window.width,
        borderBottomWidth: 0,
      },
      headerTitle: <Header />,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      UserName: "",
      Balance: ''
    }

  }

  GotoWebWiew(page) {


    if (page == "ABOUTUS") {
      Settings.CondtionUrl = Settings.aboutus;
      Settings.CondtionHeader = "ABOUT US"
      Settings.NavigateProps.navigation.navigate("WebviewForCondtions")
    }
    if (page == "TERMSANDCONDITIONS") {
      Settings.CondtionUrl = Settings.termsAndCoditions;
      Settings.CondtionHeader = "TERMS AND CONDITIONS"
      Settings.NavigateProps.navigation.navigate("WebviewForCondtions")
    }
    if (page == "PRIVACYPOLICY") {
      Settings.CondtionUrl = Settings.PrivacyPolicy;
      Settings.CondtionHeader = "PRIVACY POLICY "
      Settings.NavigateProps.navigation.navigate("WebviewForCondtions")
    }
    if (page == "Account-details") {
      Settings.CondtionHeader = "Account Details"
      Settings.NavigateProps.navigation.navigate("Payment")
    }
    // if (page == "Credit-wallet") {

    //     Settings.nav.navigate("Creditwallet")
    // }

    if (page == "orders") {
      Settings.backfromLogin = "AccountStack";
      Settings.NavigateProps.navigation.navigate("AddVehicle")
    }
    if (page == "FAQ") {

      Settings.CondtionUrl = Settings.FAQ;
      Settings.CondtionHeader = "FAQ "
      Settings.NavigateProps.navigation.navigate("WebviewForCondtions")
    }
    if (page == "ORDERSANDSHIPPING") {
      Settings.CondtionUrl = Settings.ORDERSANDSHIPPING;
      Settings.CondtionHeader = "ORDERS AND SHIPPING "
      Settings.NavigateProps.navigation.navigate("WebviewForCondtions")
    }


  }
  handleRefresh() {
    Settings.DroverStatus = false
    Settings.category_filter = '';
    Settings.sortby_filter = '';
    Settings.Designer_filter = '';
    Settings.Gender_filter = '';
    Settings.AllPrtoductFilterName = '';

    this.setState({ UserName: Settings.userEmail })

    Api4.getPayHistory()
      .then((responseJson) => {

        if (responseJson.status == "success") {
          Settings.PaymnetHistory = responseJson.data.BalanceInfo;
          //  Settings.NavigateProps.navigation.navigate(Settings.backfromLogin),
          if (Settings.PaymnetHistory) {
            this.setState({ Balance: currencyConvertor.FormateCurrency(Settings.PaymnetHistory[0].MainBalance) })
          }
        }

        else {
          Settings.PaymnetHistory = []
          Settings.NavigateProps.navigation.navigate(Settings.backfromLogin)
        }
      })
      .catch((error) => {
        console.error(error);
      });



  }


  onPressRegister() {
    Settings.backfromLogin = "AccountStack";
    Settings.RegisterLogin = "REGISTER";
    Settings.NavigateProps.navigation.navigate("MainLoginRegister")
  }
  onPressLogin() {
    Settings.userEmail = '';
    Settings.userName = '';
    Settings.userid = '';
    Settings.LoginStatus = false;
    Settings.authorization = '';
    Settings.backfromLogin = "AccountStack";
    Settings.RegisterLogin = "LOGIN";
    Settings.NavigateProps.navigation.navigate("MainLogin")
  }


  render() {
    return (
      <View>


        <StickyHeaderFooterScrollView
          makeScrollable={true}
          fitToScreen={true}
          contentBackgroundColor={'#fff'}
          renderStickyHeader={() => (
            <View>
              {/* <GluxHome></GluxHome>
                            <SearchHome ></SearchHome> */}
              {!Settings.LoginStatus && (<View style={styles.container}>
                <TouchableOpacity onPress={this.onPressRegister} style={styles.buttonContainer}>
                  <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#FFFFFF' }}>REGISTER</Text>

                </TouchableOpacity >
                <TouchableOpacity onPress={this.onPressLogin} style={styles.buttonContainer2}>
                  <Text style={{ fontFamily: 'Helveticaregular', padding: 2, color: '#f9f9fc' }}>SIGN IN</Text>

                </TouchableOpacity >

              </View>)}
              {Settings.LoginStatus && (<View style={styles.container2}>
                <Text style={{ fontFamily: 'Helveticaregular', padding: 2, fontSize: 18, color: Colors.headerColor }}>Welcome {this.state.UserName}</Text>
                <Text style={{ fontFamily: 'Helveticaregular', padding: 2, fontSize: 18, color: Colors.headerColor }}>Main Balance: {this.state.Balance}</Text>
                {/* <Text style={{ fontFamily: 'Helvetica', padding: 2 }}>Premium Member</Text> */}
              </View>)}
            </View>
          )}






        >

          <View>
            <NavigationEvents
              onWillFocus={payload => {
                this.handleRefresh();
              }}
            />
            <View style={{ marginLeft: 10, width: windowhalfwidthSecond * 3, flexDirection: "column", borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>
              {Settings.LoginStatus && (
                <TouchableOpacity onPress={this.GotoWebWiew.bind(this, "orders")} style={{ width: '100%', }}>
                  <View style={{ marginLeft: 5, flexDirection: 'row', borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>
                    <Icon name="truck1" size={25} style={{ fontWeight: 5, paddingTop: 10, paddingBottom: 20, color: Colors.headerColor }} ></Icon>
                    <Text style={{
                      fontSize: 18,
                      fontFamily: 'Helveticaregular', width: '100%',
                      textAlign: 'left',
                      paddingTop: 10,
                      paddingBottom: 20,
                      padding: 3,
                      alignSelf: 'center',
                    }}>Add Vehicles
                                        </Text>
                  </View>

                </TouchableOpacity>
              )}
              {Settings.LoginStatus && (<TouchableOpacity onPress={this.GotoWebWiew.bind(this, "Account-details")} style={{ width: '100%', }}>
                <View style={{ marginLeft: 5, flexDirection: 'row' }}>
                  <Icon name="paypal" size={25} style={{ fontWeight: 5, paddingTop: 10, paddingBottom: 20, color: Colors.headerColor }} ></Icon>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: 'Helveticaregular', width: '100%',
                    textAlign: 'left',
                    paddingTop: 10,
                    paddingBottom: 20,
                    padding: 3,
                    alignSelf: 'center',
                  }}>Add Balance</Text>
                </View>

              </TouchableOpacity>)}



            </View>
            <View style={{ marginLeft: 10, width: windowhalfwidthSecond * 3, flexDirection: "column", borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>


              <View style={{ marginLeft: 5 }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: 'Helvetica', width: '100%',
                  textAlign: 'left',
                  paddingTop: 10,


                  alignSelf: 'center',
                }}>MY LOCATION</Text>
              </View>
              <View style={{ flexDirection: "row", marginLeft: 5 }}>

                <Text style={{
                  fontSize: 14,
                  fontFamily: 'Helvetica', width: '100%',
                  textAlign: 'left',

                  paddingTop: 2,
                  marginLeft: 3,
                  alignSelf: 'center',
                }}>Singapore, S$ SGD </Text>
              </View>
              <View style={{ flexDirection: "column", marginLeft: 5 }}>

                <Text style={{
                  fontSize: 14,
                  fontFamily: 'Helveticaregular', width: '100%',
                  textAlign: 'left',
                  paddingTop: 2,
                  marginLeft: 3,
                  color: 'gray',
                  paddingBottom: 10,
                  alignSelf: 'center',
                }}>Your chosen location define your shopping currency </Text>
              </View>


            </View>
            <View style={{ marginLeft: 10, width: windowhalfwidthSecond * 3, flexDirection: "column", borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>

              <TouchableOpacity onPress={this.GotoWebWiew.bind(this, "FAQ")} style={{ width: '100%', }}>
                <View style={{ marginLeft: 5 }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: 'Helvetica', width: '100%',
                    textAlign: 'left',
                    paddingTop: 10,
                    paddingBottom: 20,

                    alignSelf: 'center',
                  }}>FAQS</Text>
                </View>

              </TouchableOpacity>

            </View>
            <View style={{ marginLeft: 10, width: windowhalfwidthSecond * 3, flexDirection: "column", borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>

              <TouchableOpacity onPress={this.GotoWebWiew.bind(this, "ABOUTUS")} style={{ width: '100%', }}>
                <View style={{ marginLeft: 5 }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: 'Helvetica', width: '100%',
                    textAlign: 'left',
                    paddingTop: 10,
                    paddingBottom: 20,

                    alignSelf: 'center',
                  }}>ABOUT US </Text>
                </View>

              </TouchableOpacity>

            </View>
            <View style={{ marginLeft: 10, width: windowhalfwidthSecond * 3, flexDirection: "column", borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>

              <TouchableOpacity onPress={this.GotoWebWiew.bind(this, "ORDERSANDSHIPPING")} style={{ width: '100%', }}>
                <View style={{ marginLeft: 5 }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: 'Helvetica', width: '100%',
                    textAlign: 'left',
                    paddingTop: 10,
                    paddingBottom: 20,

                    alignSelf: 'center',
                  }}>ORDERS AND SHIPPING</Text>
                </View>

              </TouchableOpacity>

            </View>
            <View style={{ marginLeft: 10, width: windowhalfwidthSecond * 3, flexDirection: "column", borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>

              <TouchableOpacity onPress={this.GotoWebWiew.bind(this, "TERMSANDCONDITIONS")} style={{ width: '100%', }}>
                <View style={{ marginLeft: 5 }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: 'Helvetica', width: '100%',
                    textAlign: 'left',
                    paddingTop: 10,
                    paddingBottom: 20,

                    alignSelf: 'center',
                  }}>TERMS AND CONDITIONS </Text>
                </View>

              </TouchableOpacity>

            </View>
            <View style={{ marginLeft: 10, width: windowhalfwidthSecond * 3, flexDirection: "column", borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>

              <TouchableOpacity onPress={this.GotoWebWiew.bind(this, "PRIVACYPOLICY")} style={{ width: '100%', }}>
                <View style={{ marginLeft: 5 }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: 'Helvetica', width: '100%',
                    textAlign: 'left',
                    paddingTop: 10,
                    paddingBottom: 20,

                    alignSelf: 'center',
                  }}>PRIVACY POLICY  </Text>
                </View>

              </TouchableOpacity>

            </View>
            <View style={{ marginLeft: 10, width: windowhalfwidthSecond * 3, flexDirection: "column", borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5, }}>


              <View style={{ marginLeft: 5 }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: 'Helvetica', width: '100%',
                  textAlign: 'left',
                  paddingTop: 10,


                  alignSelf: 'center',
                }}>CONTACT US </Text>
              </View>
              <View style={{ flexDirection: "row", marginLeft: 5 }}>

                <Text style={{
                  fontSize: 14,
                  fontFamily: 'Helvetica', width: '100%',
                  textAlign: 'left',

                  paddingTop: 2,
                  marginLeft: 3,
                  alignSelf: 'center',
                }}>FOR HELP AND FEEDBACK</Text>
              </View>
              <View style={{ flexDirection: "column", marginLeft: 5 }}>

                <Text style={{
                  fontSize: 14,
                  fontFamily: 'Helveticaregular', width: '100%',
                  textAlign: 'left',
                  paddingTop: 2,
                  marginLeft: 3,
                  color: 'gray',
                  paddingBottom: 10,
                  alignSelf: 'center',
                }}>Available on Monday - Friday 10:00 AM - 05:00 PM (GMT +08) </Text>
              </View>
              <View style={{ flexDirection: "column", marginLeft: 5 }}>

                <Text style={{
                  fontSize: 14,
                  fontFamily: 'Helveticaregular', width: '100%',
                  textAlign: 'left',
                  paddingTop: 2,
                  marginLeft: 3,
                  color: 'gray',
                  paddingBottom: 10,
                  alignSelf: 'center',
                }}>Closed on Saturday and Sunday
                                </Text>
              </View>


              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity onPress={() => Communications.phonecall("+65 6570 8226", true)}>
                  <View style={{ flexDirection: "column", width: windowhalfwidthSecond - 5, justifyContent: 'center', alignItems: 'center' }}>

                    <Icon name="Call-us" size={20} style={{ fontWeight: 5, color: Colors.headerColor }} ></Icon>
                    <Text style={{
                      fontSize: 14,
                      fontFamily: 'Helvetica', width: '100%',
                      textAlign: 'left',
                      paddingTop: 2,
                      marginLeft: 3,

                      alignSelf: 'center',
                      textAlign: 'center'
                    }}>CALL US
                                        </Text>
                    <Text style={{
                      fontSize: 14,
                      fontFamily: 'Helveticaregular', width: '100%',
                      textAlign: 'left',
                      marginLeft: 3,
                      color: 'gray',
                      paddingBottom: 10,
                      alignSelf: 'center',
                      textAlign: 'center'
                    }}>+65 6570 8226
                                        </Text>

                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { Linking.openURL('http://api.whatsapp.com/send?phone=6585144978') }}>
                  <View style={{ flexDirection: "column", width: windowhalfwidthSecond - 5, justifyContent: 'center', alignItems: 'center' }}>
                    <Icon name="Whatsapp-us" size={20} style={{ fontWeight: 5, color: Colors.headerColor }} ></Icon>
                    <Text style={{
                      fontSize: 14,
                      fontFamily: 'Helvetica', width: '100%',
                      textAlign: 'left',
                      paddingTop: 2,
                      marginLeft: 3,

                      alignSelf: 'center',
                      textAlign: 'center'
                    }}>WHATSAPP US
                                        </Text>

                    <Text style={{
                      fontSize: 14,
                      fontFamily: 'Helveticaregular', width: '100%',
                      textAlign: 'left',
                      marginLeft: 3,
                      color: 'gray',
                      paddingBottom: 10,
                      alignSelf: 'center',
                      textAlign: 'center'
                    }}>85144978
                                        </Text>

                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { Linking.openURL('mailto:concierge@dinghe.sg?subject=Glux') }}>
                  <View style={{ flexDirection: "column", width: windowhalfwidthSecond - 5, justifyContent: 'center', alignItems: 'center' }}>
                    <Icon name="Email-us" size={20} style={{ fontWeight: 5, color: Colors.headerColor }} ></Icon>
                    <Text style={{
                      fontSize: 14,
                      fontFamily: 'Helvetica', width: '100%',
                      textAlign: 'left',
                      paddingTop: 2,
                      marginLeft: 3,

                      alignSelf: 'center',
                      textAlign: 'center'
                    }}>EMAIL US
                                        </Text>
                    <Text style={{
                      fontSize: 14,
                      fontFamily: 'Helveticaregular', width: '100%',
                      textAlign: 'left',
                      marginLeft: 3,
                      color: 'gray',
                      paddingBottom: 10,
                      alignSelf: 'center',
                      textAlign: 'center'
                    }}>concierge@dinghe.sg
                                        </Text>
                  </View>
                </TouchableOpacity>
              </View>






            </View>
            {Settings.LoginStatus && (<View style={{ alignItems: 'center' }}>
              <Text style={{ fontFamily: 'Helveticaregular', padding: 2, fontSize: 14, marginTop: 15 }}>Not {this.state.UserName} ?</Text>
              <View style={styles.containerButton}>

                <TouchableOpacity style={styles.buttonContainer3} onPress={this.onPressLogin} >
                  <Text style={{ fontFamily: 'Helvetica', padding: 2, color: '#f9f9fc', fontWeight: 'bold' }}>SIGN OUT</Text>
                </TouchableOpacity >
              </View>
            </View>)}

          </View>

        </StickyHeaderFooterScrollView>
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 15,
    borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5,
    paddingBottom: 15,
    marginLeft: 10, width: windowhalfwidthSecond * 3
  },
  container2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: 15,
    borderBottomColor: Colors.SelectedCategoryColor, borderBottomWidth: .5,
    paddingBottom: 15,
    marginLeft: 10, width: windowhalfwidthSecond * 3
  },
  buttonContainer: {
    backgroundColor: Colors.headerColor,
    borderRadius: 2,
    padding: 10,
    shadowColor: '#f9fafc',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 1,
    shadowOpacity: 0.25,
    borderWidth: 1, borderColor: Colors.headerColor,
    marginRight: 3,
    width: 110,
    alignItems: "center"

  },
  buttonContainer2: {
    backgroundColor: Colors.headerColor,
    borderRadius: 2,
    padding: 10,
    shadowColor: '#ffffff',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 1,
    shadowOpacity: 0.25,
    borderWidth: 1, borderColor: Colors.headerColor,
    marginLeft: 3,
    width: 110,
    alignItems: "center"
  },
  buttonContainer3: {

    backgroundColor: Colors.headerColor,
    padding: 3,
    paddingTop: 10,
    paddingBottom: 10,
    shadowColor: '#ffffff',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 1,
    shadowOpacity: 0.25,
    borderWidth: 1,
    borderColor: Colors.headerColor,
    width: '100%',
    alignItems: "center"
  }, containerButton: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 20,
    marginTop: 20
  },
});