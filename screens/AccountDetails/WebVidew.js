import React from "react";
import {Image, WebView, ScrollView, Text, TouchableOpacity, StyleSheet, View, Dimensions } from "react-native";
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import icoMoonConfig from '../../src/selection.json';
import Settings from "../../constants/Settings";
import Header from '../MainScreens/HeaderForConditionWebView'
import fontelloConfig from '../../src/config.json';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import Colors from '../../constants/Colors';
import Layout from '../../constants/Layout';


const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const Icon = createIconSetFromFontello(fontelloConfig);
export default class Accounts extends React.Component {

    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);

    }

    clickBack() {
        Settings.NavigateProps.navigation.navigate("AccountStack")

    }


    render() {
        return (
            <View>


                <StickyHeaderFooterScrollView
                    makeScrollable={true}
                    fitToScreen={true}
                    contentBackgroundColor={'#fff'}
                    renderStickyHeader={() => (

                        <View style={s.header}>
                            <View style={{ flexDirection: "row", justifyContent: 'space-between', padding: 10 }}>
                                <View style={{ height: Layout.headerHeight, justifyContent: 'center' }}><Image style={{ height: Layout.headerHeight - 5, width: Layout.window.width / 3 + 50 }} resizeMode="contain" source={require('../../assets/images/logo.png')} /></View>
                                <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}><Icon name="cancel-1" size={23} style={{ fontWeight: 5, color: "#ffff", }} /></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )}
                >

                    <View style={{ padding: 10 }}>


                        <ScrollView>
                            <WebView
                                source={{ uri: Settings.CondtionUrl }}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                startInLoadingState={false}
                                style={{ marginTop: 20, height: 5000 }}
                            />
                        </ScrollView>


                    </View>


                </StickyHeaderFooterScrollView>
            </View>

        )
    }
}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    },header: {
        backgroundColor: Colors.headerColor,
        flex: 1,
        alignSelf: 'stretch',
        height: '100%',
        width: Layout.window.width

    },
    inputAndroid: {
        color: 'red'
    },

});