import React from 'react';
import { Platform, Linking, KeyboardAvoidingView, Switch, TextInput, Dimensions, TouchableOpacity, ScrollView, View, StyleSheet, Text, Image } from 'react-native';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import icoMoonConfig from '../../src/selection.json';
import PhoneInput from 'react-native-phone-input'
import CheckBox from 'react-native-check-box';
import Api from "../../utils/Api/starter";
import { Dialog } from "react-native-simple-dialogs";
import Settings from '../../constants/Settings';
import Api2 from "../../utils/Api/vehicle";
import Colors from '../../constants/Colors'
import Layout from '../../constants/Layout'
import CurrencyFormater from '../../constants/currency_DateFormate'
import RNPickerSelect from 'react-native-picker-select';
import { Loading, EasyLoading } from 'react-native-easy-loading';

const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);

const windowwidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const windowhalfwidthSecond = Dimensions.get('window').width / 5;

const PAckageName = "";
const PackageID = "";
const VehicleType = "";
const packageLimit = "";
const PackageFree = "";
const PackageRate = "";
const PackageNormalRate = "";
const vehicles = "";
const selectedVehicle = "";

export default class Addvehicle extends React.Component {


    constructor(props) {
        super(props);
        this.state = {

            dialogVisible: false,
            dialogVisible2: false,

        };
        this.saving = this.saving.bind(this)
        Settings.NavigateProps = props;
    }



    handleInputChangeMaker = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ MakerState: true })
            this.setState({ Maker: text.trim() })
        }
        else {
            this.setState({ MakerState: false })
            this.setState({ Maker: text })
        }
    }

    handleInputChangeModel = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ ModelState: true })
            this.setState({ Model: text.trim() })
        }
        else {
            this.setState({ ModelState: false })
            this.setState({ Model: text })
        }
    }


    handleInputChangeNumber = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ NumberState: true })
            this.setState({ Number: text.trim() })
        }
        else {
            this.setState({ NumberState: false })
            this.setState({ Number: text })
        }
    }


    saving() {
        var savingstate = true
      

        if (this.state.Maker == '' || this.state.Maker == undefined || this.state.Maker == null) {
            this.setState({ MakerState: true })
            savingstate = false
        }
        if (this.state.Model == '' || this.state.Model == undefined || this.state.Model == null) {
            this.setState({ ModelState: true })
            savingstate = false
        }
        if (this.state.Number == '' || this.state.Number == undefined || this.state.Number == null) {
            this.setState({ NumberState: true })
            savingstate = false
        }

        if (savingstate == true) {
          
            setTimeout(() => {
                EasyLoading.show();
            })


            Api2.addVehicle({ maker: this.state.Maker, model: this.state.Model, number: this.state.Number})
                .then((responseJson) => {
                    console.log("responseJson", responseJson)
                    if (responseJson.message == "success") {
                        this.setState({ ResultData: responseJson.message })
                        EasyLoading.dismis();
                        this.setState({ dialogVisible: true })
                        this.interval = setInterval(() => {
                            clearInterval(this.interval);
                            this.setState({ dialogVisible: false })
                            Settings.NavigateProps.navi
                        }, 2000);

                        gation.navigate("Qrcode")
                    }
                    else {
                        this.setState({ ResultData: responseJson.message })
                        EasyLoading.dismis();
                        this.setState({ dialogVisible2: true })
                        this.interval = setInterval(() => {

                            clearInterval(this.interval);
                            this.setState({ dialogVisible2: false })

                        }, 2000);
                    }
                })
                .catch((error) => {
                    console.log("error", error);
                    this.setState({ ResultData: error.message })
                    EasyLoading.dismis();
                    this.setState({ dialogVisible2: true })
                    this.interval = setInterval(() => {

                        clearInterval(this.interval);
                        this.setState({ dialogVisible2: false })

                    }, 1000);
                });
        }
    }




    clickBack() {
        Settings.NavigateProps.navigation.navigate("AccountStack")
    }





    render() {
        return (
            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (




                    <View style={styles.header}>
                        <View style={{ flexDirection: "row", justifyContent: 'space-between', padding: 10 }}>
                            <View style={{ height: Layout.headerHeight, justifyContent: 'center' }}><Image style={{ height: Layout.headerHeight - 5, width: Layout.window.width / 3 + 50 }} resizeMode="contain" source={require('../../assets/images/logo.png')} /></View>
                            <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}><Icon name="cancel-1" size={23} style={{ fontWeight: 5, color: "#ffff", }} /></TouchableOpacity>
                            </View>
                        </View>
                    </View>

                )}

            >


                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: windowwidth }}>


                    <View style={{ width: windowwidth, backgroundColor: '#fff', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Dialog
                            visible={this.state.dialogVisible}

                            onTouchOutside={() => this.setState({ dialogVisible: false })} >
                            <View>
                                <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon color={Colors.headerColor} name="check" size={18} style={{ fontWeight: 5 }} ></Icon>   {this.state.ResultData} </Text>
                            </View>
                        </Dialog>
                        <Dialog
                            visible={this.state.dialogVisible2}

                            onTouchOutside={() => this.setState({ dialogVisible: false })} >
                            <View>
                                <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon2 color={Colors.headerColor} name="info" size={18} style={{ fontWeight: 5 }} ></Icon2>   {this.state.ResultData} </Text>
                            </View>
                        </Dialog>



                        <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, paddingBottom: 18, width: '95%', justifyContent: 'center', alignItems: 'center', padding: 2 }} >


                            <KeyboardAvoidingView

                                behavior="padding"
                            >
                                <Loading />
                                <Loading type={"type"} loadingStyle={{ backgroundColor: "#0b61ea" }} />
                                <View style={{ marginLeft: 10 }} >

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> <Icon2 color={Colors.headerColor} name="truck" size={20} style={{ fontWeight: 5 }} ></Icon2></Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Vehicle Maker*"
                                                onChangeText={this.handleInputChangeMaker}
                                                value={this.state.Maker}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.MakerState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>



                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> <Icon2 color={Colors.headerColor} name="truck1" size={20} style={{ fontWeight: 5 }} ></Icon2></Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Vehicle Model*"
                                                onChangeText={this.handleInputChangeModel}
                                                value={this.state.Model}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.ModelState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> <Icon2 color={Colors.headerColor} name="Designer" size={20} style={{ fontWeight: 5 }} ></Icon2></Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Vehicle Number*"
                                                onChangeText={this.handleInputChangeNumber}
                                                value={this.state.Number}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.NumberState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>



                                </View>
                            </KeyboardAvoidingView>
                            <View style={{ margin: 2, marginTop: 15, width: '95%', justifyContent: 'space-between', alignItems: 'center' }}>



                                <View style={styles.containerSignin}>
                                    <TouchableOpacity onPress={this.saving.bind(this)} style={styles.buttonContainerSignin}>
                                        <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20 }}>ADD VEHICLE</Text>

                                    </TouchableOpacity >
                                </View>

                            </View>

                        </View>
                    </View >


                </View>
            </StickyHeaderFooterScrollView>
        )
    }
}

const styles = StyleSheet.create({

    buttonContainerRegister: {
        shadowColor: Colors.headerColor,
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: Colors.headerColor,
        padding: 18,
        backgroundColor: Colors.headerColor,
        width: '100%',
        alignItems: "center"
    }, containerRegister: {

        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    header: {
        backgroundColor: Colors.headerColor,
        flex: 1,
        alignSelf: 'stretch',
        height: '100%',
        width: Layout.window.width

    },
    buttonContainerSignin: {
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#cccdce',
        padding: 18,
        backgroundColor: '#fff',
        width: '100%',
        alignItems: "center"
    }, containerSignin: {
        marginTop: 35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    }


});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        paddingHorizontal: 10,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        color: 'black',
    },
    underlineAndroid: { color: '#fff' },
    inputAndroid: {

        paddingHorizontal: 10,
        backgroundColor: '#fff',
    },
    viewContainer: {
        backgroundColor: 'red'
    }



});