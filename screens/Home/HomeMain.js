import React from 'react';
import { TouchableOpacity, View, Text, ScrollView, StyleSheet } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import Header from '../MainScreens/Header';
import { NavigationEvents } from "react-navigation";
import Layout from '../../constants/Layout';
import Search from '../MainScreens/Search';
import Banner from '../MainScreens/Banner';
import RecommendForYou from './RecomentadForYou';
import Login from '../LoginandRegister/Login'
import Settings from '../../constants/Settings'

export default class Homemain extends React.Component {

    static navigationOptions = ({ title, subtitle }) => {
        return {
            headerStyle: {
                height: Layout.headerHeight,
                width: Layout.window.width,
                borderBottomWidth: 0,
            },
            headerTitle: <Header />,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            buttonStatus: true
        }
        Settings.NavigateProps = props;
    }

    changeMenu() {
        this.setState({ buttonStatus: !this.state.buttonStatus })

    }
    async componentWillMount() {
        Settings.backfromLogin = "CartStack";
    }
    render() {
        return (
            <ScrollView style={styles.container}>
                <View>
                    <NavigationEvents
                        onWillFocus={payload => {

                            this.setState({ buttonStatus: true })

                        }}
                    />
                    {!Settings.LoginStatus && (<View>

                        {this.state.buttonStatus && (<View style={styles.containerRegister}>
                            <TouchableOpacity onPress={this.changeMenu.bind(this)} style={styles.buttonContainerRegister}>
                                <Text style={{ padding: 2, fontSize: 18 }}>View Packages</Text>

                            </TouchableOpacity >
                        </View>)}

                        {!this.state.buttonStatus && (<View style={styles.containerRegister}>
                            <TouchableOpacity onPress={this.changeMenu.bind(this)} style={styles.buttonContainerRegister}>
                                <Text style={{ padding: 2, fontSize: 18 }}>LOGIN</Text>

                            </TouchableOpacity >
                        </View>)}
                        {this.state.buttonStatus && (<Login></Login>)}
                        {!this.state.buttonStatus && (<RecommendForYou></RecommendForYou>)}

                    </View>)}


                    {Settings.LoginStatus && (<RecommendForYou></RecommendForYou>)}
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

        backgroundColor: '#fff',
    },
    containerRegister: {

        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    buttonContainerRegister: {
        shadowColor: "#0769b5",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderBottomWidth: 1,
        borderColor: "#0769b5",
        paddingTop: 18,
        backgroundColor: "#fff",
        alignItems: "center"
    }
});
