import React from "react";
import { Dimensions, Image, ScrollView, Text, TouchableOpacity, StyleSheet, View, ImageBackground } from "react-native";
import Api from "../../utils/Api/ApiHomepage1"
import Api2 from "../../utils/Api/GetPackages"
import Settings from "../../constants/Settings"
import currencyFormatter from "../../constants/currency_DateFormate"
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Colors from "../../constants/Colors";


const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
const width = (Dimensions.get('window').width);
const Screenwidth = (Dimensions.get('window').width);
const Screenheight = (Dimensions.get('window').height);
const windowwidth = Dimensions.get('window').width;




export default class searCatach extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            SmallTab: false,
            MedumTab: false,
            LargeTab: false,
            dataSource: []

        };

    }

    async componentWillMount() {

        Api2.packages({
        }
        )
            .then((responseJson) => {

                Settings.PackageSmall = responseJson.data.packages.filter(function (hero) {
                    return hero.type == "S";
                });

                Settings.PackageMedium = responseJson.data.packages.filter(function (hero) {
                    return hero.type == "M";
                });


                Settings.PackageLarge = responseJson.data.packages.filter(function (hero) {
                    return hero.type == "L";
                });



            })
            .catch((error) => {
                console.error(error);
            });


    }


    ClickSmallTab() {
        this.setState({ SmallTab: !this.state.SmallTab })
    }
    ClickMedumTab() {
        this.setState({ MedumTab: !this.state.MedumTab })
    }

    ClickLargeTab() {
        this.setState({ LargeTab: !this.state.LargeTab })
    }



    ProductDetails(item, id, Merchentid, ImageUrl, smallText, Price, stock) {

        Settings.NavigateProps.navigation.navigate("ProductDetails", {
            name: item, id: id, Merchentid: Merchentid
            , ImageUrl: ImageUrl, smallText: smallText, Price: Price, stock: stock
        });
        // props.navigation.navigate("buttontesting", { name: item, id: id })
    }

    render() {




        return (
            // <View  parentReference = {this.handleClick.bind(this)} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ backgroundColor: '#fff', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={styles.MainContainer}>
                    <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, borderRadius: 20 }}>
                        <View style={{ height: 50, borderRadius: 20 }}>
                            <TouchableOpacity onPress={this.ClickSmallTab.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                                <View></View>
                                <Text style={{
                                    fontSize: 20,
                                    fontFamily: 'Helvetica',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    fontWeight: 'bold'
                                }}>SMALL
                        </Text>
                                <View style={{ justifyContent: 'center', alignItems: "center" }}>
                                    {!this.state.SmallTab && (
                                        <Icon name="left-open" size={22} style={{
                                            marginRight: 15, textAlign: 'center',
                                            alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                        }} />)}

                                    {this.state.SmallTab && (
                                        <Icon name="left-open" size={22} style={{
                                            marginRight: 15, textAlign: 'center',
                                            alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                        }} />
                                    )}
                                </View>
                            </TouchableOpacity>
                        </View>


                        {this.state.SmallTab && (
                            <View>

                                {Settings.PackageSmall.map((item, index) => this.createPackages(item, index, Settings.SmallVehicleState))}




                            </View>)}
                    </View>

                    <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, marginTop: 20, borderRadius: 20 }}>
                        <View style={{ height: 50, borderRadius: 20 }}>
                            <TouchableOpacity onPress={this.ClickMedumTab.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                                <View></View>
                                <Text style={{
                                    fontSize: 20,
                                    fontFamily: 'Helvetica',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    fontWeight: 'bold'
                                }}>MEDIUM
                        </Text>
                                <View style={{ justifyContent: 'center', alignItems: "center" }}>
                                    {!this.state.MedumTab && (
                                        <Icon name="left-open" size={22} style={{
                                            marginRight: 15, textAlign: 'center',
                                            alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                        }} />)}

                                    {this.state.MedumTab && (
                                        <Icon name="left-open" size={22} style={{
                                            marginRight: 15, textAlign: 'center',
                                            alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                        }} />
                                    )}
                                </View>
                            </TouchableOpacity>
                        </View>


                        {this.state.MedumTab && (
                            <View>
                                {Settings.PackageMedium.map((item, index) => this.createPackages(item, index, Settings.MediumVehicleState))}
                            </View>)}
                    </View>


                    <View style={{ borderColor: '#c6c6c6', borderWidth: 1, width: windowwidth - 20, marginTop: 20, borderRadius: 20 }}>
                        <View style={{ height: 50, borderRadius: 20 }}>
                            <TouchableOpacity onPress={this.ClickLargeTab.bind(this)} style={{ width: '100%', justifyContent: 'space-between', alignItems: "center", flexDirection: 'row', height: 50 }}>
                                <View></View>
                                <Text style={{
                                    fontSize: 20,
                                    fontFamily: 'Helvetica',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    fontWeight: 'bold'
                                }}>LARGE
                        </Text>
                                <View style={{ justifyContent: 'center', alignItems: "center" }}>
                                    {!this.state.LargeTab && (
                                        <Icon name="left-open" size={22} style={{
                                            marginRight: 15, textAlign: 'center',
                                            alignSelf: 'center', transform: [{ rotate: '180deg' }]
                                        }} />)}

                                    {this.state.LargeTab && (
                                        <Icon name="left-open" size={22} style={{
                                            marginRight: 15, textAlign: 'center',
                                            alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                        }} />
                                    )}
                                </View>
                            </TouchableOpacity>
                        </View>


                        {this.state.LargeTab && (
                            <View>
                                {Settings.PackageLarge.map((item, index) => this.createPackages(item, index, Settings.LargeVehicleState))}
                            </View>)}
                    </View>

                </View>
            </View>
        );
    }

    createPackages(item, index, statee,type) {

        return (<View key={index} style={{ flexDirection: "column" }}>
            {statee != 0 && (

                <TouchableOpacity style={{ borderRadius: 20, shadowColor: '#c6c6c6', shadowOpacity: 3.0, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'column', overflow: "visible", justifyContent: 'center', margin: 5, marginBottom: 10, padding: 5 }} onPress={() =>(Settings.shopallBackbuttonPage='HomeStack', Settings.NavigateProps.navigation.navigate('AddPackages', { VehicleType:item.type,PackageID:item._id,PackageName: item.name,packageLimit:item.limit,PackageFree:item.free,PackageRate:item.rate,PackageNormalRate:item.normalrate}))}>
                <Text style={{
                    fontSize: 18,
                    textAlign: 'center',
                    alignSelf: 'center',
                    fontWeight: 'bold',

                }}>{item.name}</Text>
                <Text style={{
                    fontSize: 16,
                    textAlign: 'center',
                    alignSelf: 'center',

                }}>{item.limit} time then {item.free} Free Wash</Text>
                <Text style={{
                    fontSize: 16,
                    textAlign: 'center',
                    alignSelf: 'center',
                    color: '#ff6666'

                }}>Nomal price RM{item.normalrate}</Text>


            </TouchableOpacity>
            )}



            {statee == 0 && (
                <View style={{ borderRadius: 20, shadowColor: '#c6c6c6', shadowOpacity: 3.0, borderColor: '#c6c6c6', borderWidth: 1, flexDirection: 'column', overflow: "visible", justifyContent: 'center', margin: 5, marginBottom: 10, padding: 5 }}>
                <Text style={{
                    fontSize: 18,
                    textAlign: 'center',
                    alignSelf: 'center',
                    fontWeight: 'bold',

                }}>{item.name}</Text>
                <Text style={{
                    fontSize: 16,
                    textAlign: 'center',
                    alignSelf: 'center',

                }}>{item.limit} time then {item.free} Free Wash</Text>
                <Text style={{
                    fontSize: 16,
                    textAlign: 'center',
                    alignSelf: 'center',
                    color: '#ff6666'

                }}>Nomal price RM{item.normalrate}</Text>


            </View>
            )}



       </View>)
    }


}


const styles = StyleSheet.create({

    MainContainer: {


        width: (width) - 10,

        overflow: "visible",
        marginBottom: 50,
        marginTop: 15,

    },

    imageView: {

        width: '50%',
        height: 100,
        marginRight: 7,
        alignItems: "center",
        margin: 7

    },

    textView: {

        width: '65%',
        textAlignVertical: 'center',
        padding: 10,
        color: '#000'

    },

});