
import React from 'react';
import { ScrollView, Image, Platform, TouchableOpacity, KeyboardAvoidingView, ActivityIndicator, Dimensions, View, StyleSheet, Text, TextInput } from 'react-native';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import Settings from '../../constants/Settings';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Api from "../../utils/Api/starter";
import Api3 from "../../utils/Api/vehicle"
import Api4 from "../../utils/Api/GetPayHistory"
import { Dialog } from "react-native-simple-dialogs";
import Api2 from "../../utils/Api/accountUpdates";
import Colors from '../../constants/Colors'
import Layout from '../../constants/Layout'

const Icon = createIconSetFromFontello(fontelloConfig);
const windowhalfwidthSecond = Dimensions.get('window').width / 5;
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);

const windowwidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


export default class MainLogin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            isLoading: true,
            EmailState: false,
            Email: '',
            EmailMsg: '*Required',
            PasswordState: false,
            Password: '',
            dialogVisible2: false,


        };

        this.starter = this.starter.bind(this)
    }

    handleInputChangePassword = (text) => {
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ PasswordState: true })
            this.setState({ Password: text.trim() })
        }
        else {
            this.setState({ PasswordState: false })
            this.setState({ Password: text })
        }
    }
    handleInputChangeEmail = (text) => {

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ EmailState: true })
            this.setState({ Email: text.trim() })
            this.setState({ EmailMsg: 'Email is requerd' })
        }
        else {
            this.setState({ EmailState: false })
            this.setState({ Email: text })
        }
    }
    ClickRegister() {
        Settings.RegisterLogin = "REGISTER";
        Settings.NavigateProps.navigation.navigate("MainLoginRegister")
    }
    Clickstarter() {
        var savingstate = true
        if (this.state.Password == '' || this.state.Password == undefined || this.state.Password == null) {
            this.setState({ PasswordState: true })
            savingstate = false
        }
        if (this.state.Email == '' || this.state.Email == undefined || this.state.Email == null) {
            this.setState({ EmailState: true })
            this.setState({ EmailMsg: "Requerd*" })
            savingstate = false
        }
        if (savingstate == true) {

            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(this.state.Email) === false) {
                this.setState({ EmailMsg: "Must be a valid email address" })
                this.setState({ EmailState: true })
                savingstate = false
            }
            if (savingstate == true) { this.starter() }
        }
    }


    setUseName() {

       
                Api4.getPayHistory()
                    .then((responseJson) => {
                       
                        if (responseJson.status == "success") { Settings.PaymnetHistory = responseJson.data.BalanceInfo; Settings.NavigateProps.navigation.navigate(Settings.backfromLogin) }

                        else {
                            Settings.PaymnetHistory = []
                            Settings.NavigateProps.navigation.navigate(Settings.backfromLogin)
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    });

           

    }


    starter() {


        Api.Starter(this.state.Email, this.state.Password)
            .then((responseJson) => {
                console.log(responseJson);
                if (responseJson.status == "success") {

                    Settings.authorization = responseJson.data.token;
                    // console.log("Auth,",  Settings.authorization )
                    Settings.userid = responseJson.data.user._id;
                    Settings.userEmail = responseJson.data.user.email;
                    Settings.userName = responseJson.data.user.name
                    Settings.LoginStatus = true;
                    Settings.NavigateProps.navigation.navigate(Settings.backfromLogin)
                     this.setUseName();
                    //check user wise vehicle status;
                    Api3.findVehicle({
                    }
                    )
                        .then((responseJson) => {

                            Settings.smallVehicle = responseJson.data.filter(function (vehicle) {
                                return vehicle.vehicletype == "S" && vehicle.status == true;
                            })
                            Settings.SmallVehicleState = Settings.smallVehicle.length
                            Settings.MediumVehicle = responseJson.data.filter(function (vehicle) {
                                return vehicle.vehicletype == "M" && vehicle.status == true;
                            })
                            Settings.MediumVehicleState = Settings.MediumVehicle.length
                            Settings.LargeVehicle = responseJson.data.filter(function (vehicle) {
                                return vehicle.vehicletype == "L" && vehicle.status == true;
                            })
                            Settings.LargeVehicleState = Settings.LargeVehicle.length

                            Settings.ActiveVehiclelist = responseJson.data.filter(function (vehicle) {
                                return vehicle.status == true;
                            })



                        })
                        .catch((error) => {
                            console.error(error);
                        });



                }
                else {
                    this.setState({ ResultData: responseJson.message })
                    this.setState({ dialogVisible2: true })

                    this.interval = setInterval(() => {

                        clearInterval(this.interval);

                        this.setState({ dialogVisible2: false })

                    }, 2000);
                }
            })
            .catch((error) => {
                console.log("error", error);
                this.setState({ ResultData: error.message })
                this.setState({ dialogVisible2: true })
                this.interval = setInterval(() => {

                    clearInterval(this.interval);

                    this.setState({ dialogVisible2: false })

                }, 2000);
            });
    }


    clickBack() {

        Settings.NavigateProps.navigation.navigate(Settings.backfromLogin)

    }





    render() {
        return (


            <ScrollView
            >


                <View style={{ width: windowwidth, backgroundColor: '#ffff', justifyContent: 'space-between', alignItems: 'center', marginTop: 25 }}>
                    <Dialog
                        visible={this.state.dialogVisible2}

                        onTouchOutside={() => this.setState({ dialogVisible2: false })} >
                        <View>
                            <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon2 color={Colors.headerColor} name="info" size={18} style={{ fontWeight: 5 }} ></Icon2>   {this.state.ResultData} </Text>
                        </View>
                    </Dialog>


                    <View style={{ margin: 5, paddingBottom: 4, width: '95%', justifyContent: 'center', alignItems: 'center', }} >

                        <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 5, width: '100%' }}>
                            <Text style={{
                                width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                    ios: {
                                        marginTop: 0
                                    },
                                    android: {
                                        marginTop: 3

                                    }

                                })
                            }}> <Icon2 color={Colors.headerColor} name="Email-address" size={15} style={{ fontWeight: 5 }} ></Icon2></Text>
                            <View style={{ flexDirection: 'column', width: '78%' }}>
                                <TextInput
                                    placeholder="Email Address*"
                                    onChangeText={this.handleInputChangeEmail}
                                    value={this.state.Email}
                                    style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                    underlineColorAndroid="transparent"
                                />
                                {!!this.state.EmailState && (<Text style={{ marginLeft: 6, color: 'red' }}>{this.state.EmailMsg}</Text>)}
                            </View>
                        </View>
                        <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                            <Text style={{
                                width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                    ios: {
                                        marginTop: 0
                                    },
                                    android: {
                                        marginTop: 3

                                    }

                                })
                            }}> <Icon2 color={Colors.headerColor} name="Password" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>
                            <View style={{ flexDirection: 'column', width: '78%' }}>
                                <TextInput
                                    placeholder="Password*"
                                    secureTextEntry
                                    autoCorrect={false}
                                    onChangeText={this.handleInputChangePassword}
                                    value={this.state.Password}
                                    style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                    underlineColorAndroid="transparent"
                                />
                                {!!this.state.PasswordState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                            </View>
                        </View>

                    </View>

                    <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ margin: 2, marginTop: 2, width: '95%', justifyContent: 'space-between', alignItems: 'center', marginBottom: 5 }}>
                            <View style={s.containerRegister}>
                                <TouchableOpacity onPress={this.Clickstarter.bind(this)} style={s.buttonContainerRegister}>
                                    <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20, color: '#f9f9fc' }}>SIGN IN</Text>

                                </TouchableOpacity >
                            </View>


                            <View style={s.containerSignin}>
                                <TouchableOpacity onPress={this.ClickRegister} style={s.buttonContainerSignin}>
                                    <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20 }}>REGISTER</Text>

                                </TouchableOpacity >
                            </View>



                        </View>


                    </ View>


                </View>
            </ScrollView>
        )
    }


}

const s = StyleSheet.create({
    buttonContainer1: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '48%',
        alignItems: "center"
    },
    buttonContainer: {

        backgroundColor: '#39393a',
        padding: 3,
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#414142',
        width: '100%',
        alignItems: "center"
    }, containerButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20
    }, header: {
        height: 70,
        marginTop: 20,
        backgroundColor: Colors.headerColor,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 2,
    },
    inputAndroid: {
        color: 'red'
    },
    buttonContainerRegister: {
        shadowColor: Colors.headerColor,
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: Colors.headerColor,
        padding: 18,
        backgroundColor: Colors.headerColor,
        width: '100%',
        alignItems: "center"
    }, containerRegister: {

        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },

    buttonContainerSignin: {
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#cccdce',
        padding: 18,
        backgroundColor: '#fff',
        width: '100%',
        alignItems: "center"
    },

    containerSignin: {
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },


});
