
import React from "react";
import { ImageBackground, StyleSheet, View, Image, Dimensions } from "react-native";
import Carousel from 'react-native-banner-carousel';
import Colors from "../../constants/Colors";

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = Dimensions.get('window').height / 3;

const images = [
    "https://glux.sg/assets/image/mobile1.jpg",
    "https://glux.sg/assets/image/mobile2.jpg",
    "https://glux.sg/assets/image/mobile3.jpg"

];



export default class Banner extends React.Component {
    renderPage(image, index) {
        var aaa = Math.random()
      
        return (
            <View key={index}>
                {/* <ImageBackground resizeMode="contain" source={{ uri: image + "?aa=" + aaa }} style={{ height: BannerHeight, width:BannerWidth }}> 

                </ImageBackground> */}

                {/* <Image
                    resizeMode="stretch"
                    source={require('../../assets/images/New_folder/123.jpg')}
                    style={styles.CardHeadPaypall}
                /> */}
                <Image resizeMode="stretch" style={{ height: BannerHeight, width:BannerWidth }} source={{ uri: image + "?aa=" + aaa }} />
              
                {/* <ImageBackground resizeMode="cover" source={{uri:'https://shopify-customerio.s3.amazonaws.com/tools/image_attachment/image/custom_resized_3f489cec-010f-4267-a52e-a8d395c4e541.jpg'}} style={{ height: BannerHeight, width:BannerWidth }}>

                </ImageBackground> */}

            </View >
        );
    }

    render() {

        return (
            <View style={styles.container}>

                <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={0}
                    pageSize={BannerWidth}
                    removeClippedSubviews={false}
                    enableMomentum
                    initialNumToRender={1}
                    pageIndicatorContainerStyle={styles.pageIndicatorContainerStyle}
                    activePageIndicatorStyle={styles.activePageIndicatorStyle}
                    pageIndicatorStyle={styles.pageIndicatorStyle}
                >
                    {images.map((image, index) => this.renderPage(image, index))}
                </Carousel>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',   
         
    },

    activePageIndicatorStyle: {
        backgroundColor: Colors.headerColor,
       
       
    },
    
    pageIndicatorStyle: {
        backgroundColor: Colors.SelectedSubCategoryColor,
        
    },
   

});