import React from 'react';
import { ActivityIndicator, Animated, TouchableOpacity, ScrollView, StyleSheet, View, Text, Image } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import Colors from '../../constants/Colors';
import Layout from '../../constants/Layout';
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Settings from '../../constants/Settings'

const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

        };

    }


    // async componentWillMount() {
    //     this.AnimatedValue = new Animated.Value(0)
    // }
    // async componentDidMount() {
    //     Animated.timing(this.AnimatedValue, {
    //         toValue: 1500000000000000000,
    //         duration: 800

    //     }).start()
    // }
    navigatetocategory() {

        Settings.NavigateProps.navigation.navigate('CategoryPage', { SelectedCateg: Settings.DefaultCategoryForSelecting, indexx: 0, urll: Settings.DefaultUrl })


    }



    render() {


        // const interpolateRotation = this.AnimatedValue.interpolate({
        //     inputRange: [0, 1],
        //     outputRange: ['0rad', '10000000000rad'],
        // })

        // const AnimatedStyle = {
        //     transform: [{
        //         rotate: interpolateRotation
        //     }

        //     ]
        // }
        return (
            <View style={styless.header}>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row', paddingLeft: 5, paddingRight: 5 }}>
                    <View style={{ height: Layout.headerHeight, justifyContent: 'center' }}>
                     
                    </View>
                    <View style={{ height: Layout.headerHeight, justifyContent: 'center' }}><Image style={{ height: Layout.headerHeight - 5, width: Layout.window.width / 3 + 50 }} resizeMode="contain" source={require('../../assets/images/logo.png')} /></View>
                    <View style={{ height: Layout.headerHeight, justifyContent: 'center' }}></View>
                </View>

            </View >
        );
    }



}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});

const styless = StyleSheet.create({
    header: {
        backgroundColor: Colors.headerColor,
        flex: 1,
        alignSelf: 'stretch',
        height: '100%',
        width: Layout.window.width

    },
    title: {
        fontSize: 20,
        color: 'blue',
        fontWeight: 'bold',
    },
    subtitle: {
        fontSize: 16,
        color: 'purple',
        fontWeight: 'bold',
    },
});
