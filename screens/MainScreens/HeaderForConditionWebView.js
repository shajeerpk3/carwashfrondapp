import React from 'react';
import { TouchableOpacity, ScrollView, StyleSheet, View, Text,Image } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import Colors from '../../constants/Colors';
import Layout from '../../constants/Layout';
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';
import Settings from '../../constants/Settings'

const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
export default class HeaderWithOutLOgo extends React.Component {

    render() {
        return (
           // Settings.NavigateProps.navigation.navigate('Main')
            <View style={styless.header}>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row' ,paddingLeft:5,paddingRight:5}}>
                <TouchableOpacity onPress={() =>Settings.NavigateProps.navigation.navigate('AccountStack')}><View style={{ height: Layout.headerHeight, justifyContent:'center',marginLeft:5}}><Icon name="left-open" size={30} style={{ fontWeight: 5,marginTop:3 }} /></View></TouchableOpacity>
                    <View style={{ height: Layout.headerHeight, justifyContent:'center' }}><Text style={{
                                fontSize: 26,
                                fontFamily: 'Helvetica', width: '100%',
                                textAlign: 'center',
                               
                                alignSelf: 'center',
                            }}>{Settings.CondtionHeader}</Text></View>
                    <View style={{ height: Layout.headerHeight, justifyContent:'center' }}><TouchableOpacity><Icon2 name="Product-alert" size={25} style={{ fontWeight: 5 }} /></TouchableOpacity></View>
                </View>

            </View>
        );
    }



}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});

const styless = StyleSheet.create({
    header: {
        backgroundColor: Colors.headerColor,
      
      
        height: Layout.headerHeight,
        width:Layout.window.width

    },
    title: {
        fontSize: 20,
        color: 'blue',
        fontWeight: 'bold',
    },
    subtitle: {
        fontSize: 16,
        color: 'purple',
        fontWeight: 'bold',
    },
});
