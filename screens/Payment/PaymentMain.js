import React from 'react';
import { Platform, KeyboardAvoidingView, TextInput, Dimensions, TouchableOpacity, ScrollView, View, StyleSheet, Text, Image } from 'react-native';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import icoMoonConfig from '../../src/selection.json';
import PhoneInput from 'react-native-phone-input'
import Api from "../../utils/Api/starter";
import { Dialog } from "react-native-simple-dialogs";
import Settings from '../../constants/Settings';
import Api2 from "../../utils/Api/GetPackages";
import Colors from '../../constants/Colors'
import Layout from '../../constants/Layout'
import CurrencyFormater from '../../constants/currency_DateFormate'
import RNPickerSelect from 'react-native-picker-select';
import { Loading, EasyLoading } from 'react-native-easy-loading';

const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);

const windowwidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const windowhalfwidthSecond = Dimensions.get('window').width / 5;



export default class AddPackages extends React.Component {


    constructor(props) {
        super(props);
        this.state = {


            dialogVisible: false,
            dialogVisible2: false,


        };
        this.saving = this.saving.bind(this)
        Settings.NavigateProps = props;
    }









    async componentWillMount() {



    }


    saving() {
        // Settings.NavigateProps.navigation.navigate("Paypal")
        var savingstate = true;
       
        if (this.state.Name == '' || this.state.Name == undefined || this.state.Name == null) {
            this.setState({ NameState: true })
            savingstate = false
        }
        if (this.state.Amount == '' || this.state.Amount == undefined || this.state.Amount == null) {
            this.setState({ AmountState: true })
            savingstate = false
        }
        if (this.state.Address == '' || this.state.Address == undefined || this.state.Address == null) {
            this.setState({ AddressState: true })
            savingstate = false
        }

        if (this.state.Addressline2 == '' || this.state.Addressline2 == undefined || this.state.Addressline2 == null) {
            this.setState({ Addressline2State: true })
            savingstate = false
        }
        if (this.state.Zip == '' || this.state.Zip == undefined || this.state.Zip == null) {
            this.setState({ ZipState: true })
            savingstate = false
        }

        if (this.state.Mob == '' || this.state.Mob == undefined || this.state.Mob == null) {
            this.setState({ MobState: true })
            savingstate = false
        }
        if(savingstate==true){
            Settings.PaypalNetTotal= this.state.Amount;
            Settings.BillTo=this.state.Name;
            Settings.address=this.state.Address;
            Settings.UnitNum=this.state.Addressline2;
            Settings.PostalCode=this.state.Zip;
            Settings.ContactNum=this.state.Mob

            Settings.NavigateProps.navigation.navigate("Paypal")
        }
    }




    clickBack() {
        Settings.NavigateProps.navigation.navigate("AccountStack")
    }

    handleInputChangeAmount = (text) => {
        text = text.replace(/[^(((\d)+\d)|((\d)+))]/g, '')
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ AmountState: true })
            this.setState({ Amount: text.trim() })
        }
        else {
            this.setState({ AmountState: false })
            this.setState({ Amount: text.trim() })
        }

    }


    handleInputChangeName = (text) => {

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ NameState: true })
            this.setState({ Name: text })
        }
        else {
            this.setState({ NameState: false })
            this.setState({ Name: text })
        }

    }


    handleInputChangeAddress = (text) => {

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ AddressState: true })
            this.setState({ Address: text })
        }
        else {
            this.setState({ AddressState: false })
            this.setState({ Address: text })
        }

    }


    handleInputChangeAddressline2 = (text) => {

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ Addressline2State: true })
            this.setState({ Addressline2: text })
        }
        else {
            this.setState({ Addressline2State: false })
            this.setState({ Addressline2: text })
        }

    }


    handleInputChangeZip = (text) => {
        text = text.replace(/[^(((\d)+\d)|((\d)+))]/g, '')
        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ ZipState: true })
            this.setState({ Zip: text.trim() })
        }
        else {
            this.setState({ ZipState: false })
            this.setState({ Zip: text.trim() })
        }

    }


    handleInputChangeMob = (text) => {

        text = text.replace(/[^(((\d)+\d)|((\d)+))]/g, '')

        if (text.trim() == '' || text.trim() == undefined || text.trim() == null) {
            this.setState({ MobState: true })
            this.setState({ Mob: text.trim() })
        }
        else {
            this.setState({ MobState: false })
            this.setState({ Mob: text.trim() })
        }

    }


    render() {
        return (
            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (




                    <View style={styles.header}>
                        <View style={{ flexDirection: "row", justifyContent: 'space-between', padding: 10 }}>
                            <View style={{ height: Layout.headerHeight, justifyContent: 'center' }}><Image style={{ height: Layout.headerHeight - 5, width: Layout.window.width / 3 + 50 }} resizeMode="contain" source={require('../../assets/images/logo.png')} /></View>
                            <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}><Icon name="cancel-1" size={23} style={{ fontWeight: 5, color: "#ffff", }} /></TouchableOpacity>
                            </View>
                        </View>
                    </View>

                )}

            >


                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: windowwidth }}>


                    <View style={{ width: windowwidth, backgroundColor: '#fff', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Dialog
                            visible={this.state.dialogVisible}

                            onTouchOutside={() => this.setState({ dialogVisible: false })} >
                            <View>
                                <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon color={Colors.headerColor} name="check" size={18} style={{ fontWeight: 5 }} ></Icon>   {this.state.ResultData} </Text>
                            </View>
                        </Dialog>
                        <Dialog
                            visible={this.state.dialogVisible2}

                            onTouchOutside={() => this.setState({ dialogVisible: false })} >
                            <View>
                                <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon2 color={Colors.headerColor} name="info" size={18} style={{ fontWeight: 5 }} ></Icon2>   {this.state.ResultData} </Text>
                            </View>
                        </Dialog>



                        <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, paddingBottom: 18, width: '95%', justifyContent: 'center', alignItems: 'center', padding: 10 }} >


                            <KeyboardAvoidingView
                                behavior="padding"
                            >
                                <Loading />
                                <Loading type={"type"} loadingStyle={{ backgroundColor: "#0b61ea" }} />


                                <View style={{ marginLeft: 10 }} >
                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> <Icon2 color={Colors.headerColor} name="banknote, check, bill, money, cash" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Enter Amount*"
                                                onChangeText={this.handleInputChangeAmount}
                                                keyboardType='decimal-pad'
                                                value={this.state.Amount}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.AmountState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> <Icon2 color={Colors.headerColor} name="Account" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Enter Name*"
                                                onChangeText={this.handleInputChangeName}
                                                value={this.state.Name}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.NameState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>


                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> <Icon2 color={Colors.headerColor} name="address-card-o, vcard-o" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Enter Address*"
                                                onChangeText={this.handleInputChangeAddress}
                                                value={this.state.Address}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.AddressState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>


                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> </Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Address line 2*"
                                                onChangeText={this.handleInputChangeAddressline2}
                                                value={this.state.Addressline2}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.Addressline2State && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> <Icon2 color={Colors.headerColor} name="zip, compress, file" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Enter Zip Code*"
                                                onChangeText={this.handleInputChangeZip}
                                                keyboardType='numeric'
                                                value={this.state.Zip}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.ZipState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            width: '13%', fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}> <Icon2 color={Colors.headerColor} name="Call-us" size={22} style={{ fontWeight: 5 }} ></Icon2></Text>
                                        <View style={{ flexDirection: 'column', width: '78%' }}>

                                            <TextInput
                                                placeholder="Enter Mobile Number*"
                                                onChangeText={this.handleInputChangeMob}
                                                keyboardType='phone-pad'
                                                value={this.state.Mob}
                                                style={{ width: '100%', borderBottomWidth: 1, borderColor: 'gray', padding: 3, margin: 6, alignSelf: 'center', marginRight: 5 }}
                                                underlineColorAndroid="transparent"
                                            />
                                            {!!this.state.MobState && (<Text style={{ marginLeft: 6, color: 'red' }}>*Required</Text>)}
                                        </View>
                                    </View>
                                </View>




                                <View style={{ margin: 2, marginTop: 15, width: '95%', justifyContent: 'space-between', alignItems: 'center' }}>



                                    <View style={styles.containerSignin}>
                                        <TouchableOpacity onPress={this.saving.bind(this)} style={styles.buttonContainerSignin}>
                                            <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20 }}>PROCEED PAYMENT</Text>

                                        </TouchableOpacity >
                                    </View>

                                </View>
                            </KeyboardAvoidingView>

                        </View>
                    </View >


                </View>
            </StickyHeaderFooterScrollView>
        )
    }
}

const styles = StyleSheet.create({

    buttonContainerRegister: {
        shadowColor: Colors.headerColor,
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: Colors.headerColor,
        padding: 18,
        backgroundColor: Colors.headerColor,
        width: '100%',
        alignItems: "center"
    }, containerRegister: {

        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    header: {
        backgroundColor: Colors.headerColor,
        flex: 1,
        alignSelf: 'stretch',
        height: '100%',
        width: Layout.window.width

    },
    buttonContainerSignin: {
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#cccdce',
        padding: 18,
        backgroundColor: '#fff',
        width: '100%',
        alignItems: "center"
    }, containerSignin: {
        marginTop: 35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    }


});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        paddingHorizontal: 10,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        color: 'black',
    },
    underlineAndroid: { color: '#fff' },
    inputAndroid: {

        paddingHorizontal: 10,
        backgroundColor: '#fff',
    },
    viewContainer: {
        backgroundColor: 'red'
    }



});