import React, { Component } from 'react';
import { Alert, Linking, Dimensions, LayoutAnimation, Text, View, StatusBar, StyleSheet, TouchableOpacity } from 'react-native';
import { BarCodeScanner, Permissions } from 'expo';
import Header from '../MainScreens/Header';
import Layout from '../../constants/Layout';
import Settings from '../../constants/Settings'
import { Loading, EasyLoading } from 'react-native-easy-loading';


export default class Qrcode extends Component {


  constructor(props) {
    super(props);

    Settings.NavigateProps = props;
  }



  static navigationOptions = ({ title, subtitle }) => {
    return {
      headerStyle: {
        height: Layout.headerHeight,
        width: Layout.window.width,
        borderBottomWidth: 0,
      },
      headerTitle: <Header />,
    };
  };
  state = {
    hasCameraPermission: null,
    lastScannedUrl: null,
  };

  componentDidMount() {
    this._requestCameraPermission();
  }

  _requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  _handleBarCodeRead = result => {
console.log("data",result.data)
    if (result.data == "https://www.qrcode-monkey.com/PACKAGE") {

      Settings.NavigateProps.navigation.navigate("HomeStack")
    } else if(result.data == "https://www.qrcode-monkey.com/WASH"){

      Settings.NavigateProps.navigation.navigate("Washing")
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Loading />
        <Loading type={"type"} loadingStyle={{ backgroundColor: "#0b61ea" }} />
        {this.state.hasCameraPermission === null
          ? <Text>Requesting for camera permission</Text>
          : this.state.hasCameraPermission === false
            ? <Text style={{ color: '#fff' }}>
              Camera permission is not granted
                </Text>
            : <BarCodeScanner
              onBarCodeRead={this._handleBarCodeRead}
              style={{
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').width,
              }}
            />}



        <StatusBar hidden />
      </View>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
  bottomBar: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: 15,
    flexDirection: 'row',
  },
  url: {
    flex: 1,
  },
  urlText: {
    color: '#fff',
    fontSize: 20,
  },
  cancelButton: {
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancelButtonText: {
    color: 'rgba(255,255,255,0.8)',
    fontSize: 18,
  },
});