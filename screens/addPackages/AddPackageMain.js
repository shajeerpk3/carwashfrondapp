import React from 'react';
import { Platform, Linking, KeyboardAvoidingView, Switch, TextInput, Dimensions, TouchableOpacity, ScrollView, View, StyleSheet, Text, Image } from 'react-native';
import { createIconSetFromFontello, createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import icoMoonConfig from '../../src/selection.json';
import PhoneInput from 'react-native-phone-input'
import CheckBox from 'react-native-check-box';
import Api from "../../utils/Api/starter";
import { Dialog } from "react-native-simple-dialogs";
import Settings from '../../constants/Settings';
import Api2 from "../../utils/Api/GetPackages";
import Colors from '../../constants/Colors'
import Layout from '../../constants/Layout'
import CurrencyFormater from '../../constants/currency_DateFormate'
import RNPickerSelect from 'react-native-picker-select';
import { Loading, EasyLoading } from 'react-native-easy-loading';

const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);

const windowwidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const windowhalfwidthSecond = Dimensions.get('window').width / 5;

const PAckageName = "";
const PackageID = "";
const VehicleType = "";
const packageLimit = "";
const PackageFree = "";
const PackageRate = "";
const PackageNormalRate = "";
const vehicles = "";
const selectedVehicle = "";

export default class AddPackages extends React.Component {


    constructor(props) {
        super(props);
        this.state = {

            PhoneNumberState: false,
            PhoneNumber: '',

            dialogVisible: false,
            dialogVisible2: false,
            ResultData: 'saved',

        };
        this.saving = this.saving.bind(this)
        Settings.NavigateProps = props;
    }









    async componentWillMount() {
        this.PAckageName = Settings.NavigateProps.navigation.state.params.PackageName
        this.VehicleType = Settings.NavigateProps.navigation.state.params.VehicleType
        this.PackageID = Settings.NavigateProps.navigation.state.params.PackageID
        this.packageLimit = Settings.NavigateProps.navigation.state.params.packageLimit
        this.PackageFree = Settings.NavigateProps.navigation.state.params.PackageFree
        this.PackageRate = Settings.NavigateProps.navigation.state.params.PackageRate
        this.PackageNormalRate = Settings.NavigateProps.navigation.state.params.PackageNormalRate

        switch (this.VehicleType) {
            case "S": this.VehicleType = "Small"; this.vehicles = Settings.smallVehicle; break;
            case "M": this.VehicleType = "Medium"; this.vehicles = Settings.MediumVehicle; break;
            case "L": this.View = "Large"; this.vehicles = Settings.LargeVehicle; break;
        }

        this.selectedVehicle = this.vehicles[0].label;


    }


    saving() {
        setTimeout(() => {
            EasyLoading.show();
        })

        Api2.AddPackages({ packageId: this.PackageID, packageName: this.PAckageName, vehicleNumber: this.selectedVehicle, limit: this.packageLimit, complete: 0, free: this.PackageFree, PackageAmount: this.PackageNormalRate,Amount:this.PackageRate,AmountAction:"deduct",Action:"Package",PayRef:null,PayMode:null,VehicleNO:this.selectedVehicle,PackageName:this.PAckageName,AddOnId:null,AddOnName:null,WashingId:null })
            .then((responseJson) => {
                console.log("responseJson", responseJson)
                if (responseJson.message == "success") {
                    this.setState({ ResultData: responseJson.message })
                    EasyLoading.dismis();
                    this.setState({ dialogVisible: true })
                    this.interval = setInterval(() => {
                        clearInterval(this.interval);
                        this.setState({ dialogVisible: false })
                      
                    }, 2000).then( Settings.NavigateProps.navigation.navigate("Qrcode"));

                   
                }
                else {
                    this.setState({ ResultData: responseJson.message })
                    EasyLoading.dismis();
                    this.setState({ dialogVisible2: true })
                    this.interval = setInterval(() => {

                        clearInterval(this.interval);
                        this.setState({ dialogVisible2: false })

                    }, 2000);
                }
            })
            .catch((error) => {
                console.log("error", error);
                this.setState({ ResultData: error.message })
                EasyLoading.dismis();
                this.setState({ dialogVisible2: true })
                this.interval = setInterval(() => {

                    clearInterval(this.interval);
                    this.setState({ dialogVisible2: false })

                }, 1000);
            });
    }




    clickBack() {
        Settings.NavigateProps.navigation.navigate("HomeStack")
    }





    render() {
        return (
            <StickyHeaderFooterScrollView
                makeScrollable={true}
                fitToScreen={true}
                contentBackgroundColor={'#fff'}

                renderStickyHeader={() => (




                    <View style={styles.header}>
                        <View style={{ flexDirection: "row", justifyContent: 'space-between', padding: 10 }}>
                            <View style={{ height: Layout.headerHeight, justifyContent: 'center' }}><Image style={{ height: Layout.headerHeight - 5, width: Layout.window.width / 3 + 50 }} resizeMode="contain" source={require('../../assets/images/logo.png')} /></View>
                            <View style={{ width: windowhalfwidthSecond, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={this.clickBack.bind(this)} style={{ width: '100%', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}><Icon name="cancel-1" size={23} style={{ fontWeight: 5, color: "#ffff", }} /></TouchableOpacity>
                            </View>
                        </View>
                    </View>

                )}

            >


                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: windowwidth }}>


                    <View style={{ width: windowwidth, backgroundColor: '#fff', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Dialog
                            visible={this.state.dialogVisible}

                            onTouchOutside={() => this.setState({ dialogVisible: false })} >
                            <View>
                                <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon color={Colors.headerColor} name="check" size={18} style={{ fontWeight: 5 }} ></Icon>   {this.state.ResultData} </Text>
                            </View>
                        </Dialog>
                        <Dialog
                            visible={this.state.dialogVisible2}

                            onTouchOutside={() => this.setState({ dialogVisible: false })} >
                            <View>
                                <Text style={{ fontSize: 15.5, fontFamily: 'Helveticaregular', margin: 7 }}><Icon2 color={Colors.headerColor} name="info" size={18} style={{ fontWeight: 5 }} ></Icon2>   {this.state.ResultData} </Text>
                            </View>
                        </Dialog>



                        <View style={{ margin: 5, borderColor: '#c6c6c6', borderWidth: 1, paddingBottom: 18, width: '95%', justifyContent: 'center', alignItems: 'center', padding: 10 }} >


                            <KeyboardAvoidingView

                                behavior="padding"
                            >
                                <Loading />
                                <Loading type={"type"} loadingStyle={{ backgroundColor: "#0b61ea" }} />
                                <View style={{ marginLeft: 10 }} >
                                    {this.vehicles.length != 1 && (<View style={{ justifyContent: 'space-between', margin: 2, marginTop: 15, width: '100%' }}>
                                        <Text style={{
                                            fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}>Select your vehicle:</Text>
                                        <RNPickerSelect
                                            useNativeAndroidPickerStyle={false}
                                            hideIcon={true}
                                            // value={qty}
                                            placeholder={{}}
                                            items={this.vehicles}
                                            style={{ ...pickerSelectStyles }}
                                            underlineColorAndroid="transparent"
                                            onValueChange={(Selectvalue) => {
                                                this.selectedVehicle = Selectvalue
                                                // this.ShippingChargeChange(Selectvalue, index)
                                            }}

                                            Icon={() => {
                                                return <Icon name="left-open" size={14} style={{
                                                    textAlign: 'center',
                                                    marginTop: 10,
                                                    marginRight: 5,
                                                    alignSelf: 'center', transform: [{ rotate: '270deg' }]
                                                }} />
                                            }}
                                        // onValueChange= {(value)=>{this.ValueChange.bind(this,value, cartid, merchantIndex, varianceFlag, ProductIndex, varianceindex)}}
                                        >

                                        </RNPickerSelect>
                                    </View>)}
                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '80%', justifyContent: 'space-between' }}>
                                        <Text style={{
                                            fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}>Vehicle Type: {this.VehicleType}</Text>


                                    </View>

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '80%', justifyContent: 'space-between' }}>
                                        <Text style={{
                                            fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}>Package Name: <Text style={{ color: '#595959', fontSize: 17, marginLeft: 3 }}>{this.PAckageName}</Text> </Text>


                                    </View>
                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '80%', justifyContent: 'space-between' }}>
                                        <Text style={{
                                            fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}>Rate: <Text style={{ color: '#595959', fontSize: 17, marginLeft: 3 }}>{CurrencyFormater.FormateCurrency(this.PackageRate)}</Text> </Text>


                                    </View>

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '80%', justifyContent: 'space-between' }}>
                                        <Text style={{
                                            fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}>Package Limit:<Text style={{ color: '#595959', fontSize: 17, marginLeft: 3 }}>{this.packageLimit} Time</Text> </Text>


                                    </View>

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '80%', justifyContent: 'space-between' }}>
                                        <Text style={{
                                            fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}>Free Limit:<Text style={{ color: '#595959', fontSize: 17, marginLeft: 3 }}>{this.PackageFree} Time</Text> </Text>


                                    </View>

                                    <View style={{ alignItems: 'center', flexDirection: 'row', margin: 2, marginTop: 15, width: '80%', justifyContent: 'space-between' }}>
                                        <Text style={{
                                            fontFamily: 'Helvetica', color: '#595959', fontSize: 17, fontWeight: 'bold', marginLeft: 6, ...Platform.select({
                                                ios: {
                                                    marginTop: 0
                                                },
                                                android: {
                                                    marginTop: 3

                                                }

                                            })
                                        }}>Total Wasing Count:<Text style={{ color: '#595959', fontSize: 17, marginLeft: 3 }}> {this.PackageFree + this.packageLimit} Time</Text> </Text>


                                    </View>


                                </View>
                            </KeyboardAvoidingView>
                            <View style={{ margin: 2, marginTop: 15, width: '95%', justifyContent: 'space-between', alignItems: 'center' }}>



                                <View style={styles.containerSignin}>
                                    <TouchableOpacity onPress={this.saving.bind(this)} style={styles.buttonContainerSignin}>
                                        <Text style={{ fontFamily: 'Helvetica', padding: 2, fontSize: 20 }}>ADD PACKANGES</Text>

                                    </TouchableOpacity >
                                </View>

                            </View>

                        </View>
                    </View >


                </View>
            </StickyHeaderFooterScrollView>
        )
    }
}

const styles = StyleSheet.create({

    buttonContainerRegister: {
        shadowColor: Colors.headerColor,
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: Colors.headerColor,
        padding: 18,
        backgroundColor: Colors.headerColor,
        width: '100%',
        alignItems: "center"
    }, containerRegister: {

        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    header: {
        backgroundColor: Colors.headerColor,
        flex: 1,
        alignSelf: 'stretch',
        height: '100%',
        width: Layout.window.width

    },
    buttonContainerSignin: {
        shadowColor: '#ffffff',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 1,
        shadowOpacity: 0.25,
        borderWidth: 1,
        borderColor: '#cccdce',
        padding: 18,
        backgroundColor: '#fff',
        width: '100%',
        alignItems: "center"
    }, containerSignin: {
        marginTop: 35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    }


});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        paddingHorizontal: 10,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        color: 'black',
    },
    underlineAndroid: { color: '#fff' },
    inputAndroid: {

        paddingHorizontal: 10,
        backgroundColor: '#fff',
    },
    viewContainer: {
        backgroundColor: 'red'
    }



});