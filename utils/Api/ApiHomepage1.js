import settingss from '../../constants/Settings'

const servername = settingss.serverUrl1;
const servername2 = settingss.serverUrl2;

const endUrl = {
    GetCategory: servername + "/product/category",
    GetRecommendedForYou:servername +  "/product/list",
}


function callApi(endpoint, method, body, params) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
    } catch (e) {
        bodyIsJson = false;
    }

    const fullUrl = endpoint + constructParams(params);
    // console.log(fullUrl)
    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...contentTypeHeader,
        },
        body: body,
    };

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {


            if (!response.ok) {
                return Promise.reject(json);
            }
           
            return json;
        });

}

function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}


 //#region CATEGORY


function GetCategory(params) {
    return callApi(endUrl.GetCategory, "GET", null, params);
}


 //#endregion


  //#region RECOMMENDED FOR YOU
  function GetRecommendedForYou(params) {
    return callApi(endUrl.GetRecommendedForYou, "GET", null, params);
}

  //#endregion

export default {

    GetCategory,
    GetRecommendedForYou

};