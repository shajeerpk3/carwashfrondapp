import React from "react";
import settingss from '../../constants/Settings';



const servername = settingss.DeliveryUrl;


const endUrl = {
    GetPricing:servername + "/api/gateway/v1/services/price",
  
    
    // TopWomenItems:servername+"/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY",
}


function callApi(endpoint, method, body, params, authToken,lastaddid=null) {
   
    let bodyIsJson = true;
    try {
        JSON.parse(body);
        // if(body==null)
        // {
        //     bodyIsJson = false;
        // }
    } catch (e) {
        bodyIsJson = false;
      //  console.log("mindulaaa",bodyIsJson)
    }

   // console.log("mindano")

    let fullUrl:string = endpoint + constructParams(params);

    // if(lastaddid!=null){
        fullUrl=fullUrl + DeleteParam(lastaddid);
    // }

    const authHeader = authToken
        ? {
            "simplypost-api-token": `${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };
    //console.log("hello",fetchOptions)
    // if (type == "op") {
    //     console.log("fullUrl")
    //     return fetch(fullUrl, fetchOptions)
    //         .then((response) => console.log("rerererer",response))

    // }
    // if (type != "op") {

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
    // }
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }
  
    return ("/"+ params)
}


function GetPricing(param) {
    return callApi(endUrl.GetPricing, "GET", null, param, settingss.simplypostApiToken)
}





export default {
    GetPricing,
   
};