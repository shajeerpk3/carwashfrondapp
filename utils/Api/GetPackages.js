import React from "react";
import settingss from '../../constants/Settings';
const servername = settingss.serverUrl3;
const endUrl = {
    packages: servername + "/packages/getpackage",
    AddPackages: servername + "/addPackage/add",
    getVehiclePackageHistory:servername+"/addPackage/get"

}



function callApi(endpoint, method, body, params, authToken, lastaddid = null) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);

    } catch (e) {
        console.log("not jason")
        bodyIsJson = false;

    }


    let fullUrl: string = endpoint + constructParams(params);
    fullUrl = fullUrl + DeleteParam(lastaddid);
    console.log("fullUrlll", fullUrl)
    const authHeader = authToken
        ? {
            "x-access-token": ` ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };

    console.log("ssssss", fullUrl, fetchOptions)
    return fetch(fullUrl, fetchOptions)

        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });

}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }

    return ("/" + params)
}




function packages() {


    return callApi(endUrl.packages, "GET", null, null, null)

}
function AddPackages(params) {
  return callApi(endUrl.AddPackages, "POST", null, params, settingss.authorization)

}
function getVehiclePackageHistory(params) {
    return callApi(endUrl.getVehiclePackageHistory, "GET", null, params, settingss.authorization)
  
  }


export default {
    packages,
    AddPackages,
    getVehiclePackageHistory

};