import settingss from '../../constants/Settings';
import base64 from 'react-native-base64'

const Url = {
    // BasicAuthorization: "https://api.paypal.com/v1/oauth2/token?grant_type=client_credentials",//live
    BasicAuthorization: "https://api.sandbox.paypal.com/v1/oauth2/token?grant_type=client_credentials",//sandbox
}

function callApi(endpoint, method, body, params, authToken,lastaddid=null) {
   
    let bodyIsJson = true;

    const authHeader = authToken
        ? {
            "Authorization": `Basic ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Accept": "application/json",
        }
        : {};
       
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,           
            ...contentTypeHeader,
        },
        
    };
  

    return fetch(endpoint, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }
            return json;
        });
   
}








function BasicAuthorization(email,password) {
    var PAYPAL_CLIENT=settingss.PAYPAL_CLIENT
    var PAYPAL_SECRET=settingss.PAYPAL_SECRET
    basicAuth =base64.encode  (`${ PAYPAL_CLIENT }:${ PAYPAL_SECRET }`);
   
    const body = JSON.stringify({      
        grant_type:"client_credentials"
    });

    return callApi(Url.BasicAuthorization, "POST", body, null, basicAuth)
}



export default {
    BasicAuthorization,  
};
