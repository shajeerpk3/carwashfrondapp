import React from "react";
import settingss from '../../constants/Settings';


const servername = settingss.serverUrl1;
const LocalHost = settingss.loacalServer;

const endUrl = {
    
    // PaypallPaymnet: "https://api.paypal.com/v1/payments/payment",//live
    PaypallPaymnet: "https://api.sandbox.paypal.com/v1/payments/payment",//sand box

}


function callApi(endpoint, method, data, params, authToken, lastaddid = null) {

    let bodyIsJson = true;
    try {
        JSON.parse(data);
      
    } catch (e) {
        bodyIsJson = false;
        //  console.log("mindulaaa",bodyIsJson)
    }

    // console.log("mindano")

    let fullUrl: string = endpoint + constructParams(params);

    // if(lastaddid!=null){
    fullUrl = fullUrl + DeleteParam(lastaddid);



    const authHeader = authToken
        ? {
            Authorization: `Bearer ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",

        }
        : {};
    
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: JSON.stringify({
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "transactions": [
                {
                    "amount": {
                        "total":settingss.PaypalNetTotal,
                        "currency": "SGD",
                       
                       
                    },
                    "description": "Glux by gstock",

                    "payment_options": {
                        "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
                    },
                    "item_list": {
                        "items": [
                            {
                              "name": "Add Value to Main Balance",
                              "sku": "1",
                              "price": settingss.PaypalNetTotal,
                              "currency": "SGD",
                              "quantity": "1",
                            
                            }],
                    //     "items": settingss.productsinArrayPaypal,
                        "shipping_address": {
                            "recipient_name":settingss.BillTo,
                            "line1": settingss.address,
                            "line2":  settingss.UnitNum,
                            "city":"Singapore",
                            "country_code": "SG",
                            "postal_code": settingss.PostalCode,
                            "phone": settingss.ContactNum,

                        }
                    }
                }
            ],
            "note_to_payer": "Contact us for any questions on your order. +65 6570 8226",
            "redirect_urls": {
                "return_url": "http://terms.glux.sg/return_url.html",
                "cancel_url": "http://terms.glux.sg/cancel_url.html"
            }
        })
    };

        console.log("fetchOption",fetchOptions)

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
    // }
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }
    return ("/" + params)
}


function PaypallPaymnet(token) {

    
    //  console.log("settingss.PaypalNetTotal",settingss.PaypalNetTotal)
   
    //  console.log("settingss.productsinArrayPaypal",settingss.productsinArrayPaypal) 
    //  console.log("settingss.shippingName",settingss.shippingName) 
    //  console.log("  settingss.ShippingAddline1",  settingss.ShippingAddline1)
    //  console.log("settingss.ShippingAddline2", settingss.ShippingAddline2)
         
        

        
    


    return callApi(endUrl.PaypallPaymnet, "POST", null, null, token)
}





export default {
    PaypallPaymnet
};