import React from "react";
import settingss from '../../constants/Settings';
const servername = settingss.serverUrl3;
const endUrl = {
    Wash: servername + "/addPackage/washing",
   
}



function callApi(endpoint, method, body, params, authToken, lastaddid = null) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
      
    } catch (e) {
        console.log("not jason")
        bodyIsJson = false;
       
    }

  
    let fullUrl: string = endpoint + constructParams(params);
    fullUrl = fullUrl + DeleteParam(lastaddid);
     console.log("fullUrlll", fullUrl)
    const authHeader = authToken
        ? {
            "x-access-token": ` ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };
    
    console.log("ssssss", fullUrl,fetchOptions)
    return fetch(fullUrl, fetchOptions)

        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
   
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }

    return ("/" + params)
}





function Wash(parameeter){

return callApi(endUrl.Wash, "POST", null, parameeter, settingss.authorization)
}

export default {
    Wash
};