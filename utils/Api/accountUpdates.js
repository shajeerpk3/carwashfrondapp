import settingss from '../../constants/Settings';

const servername = settingss.serverUrl2;
const endUrl = {
    accountdetails: servername + "/user/account/detail",
    accountdetailsUpdate:servername + "/user/updateuser",
   
}


function callApi(endpoint, method, body, params, authToken, lastaddid = null) {

    let bodyIsJson = true;
    try {
        JSON.parse(body);
      
    } catch (e) {
        console.log("not jason")
        bodyIsJson = false;
       
    }

  
    let fullUrl: string = endpoint + constructParams(params);
    fullUrl = fullUrl + DeleteParam(lastaddid);
    // console.log("fullUrl", fullUrl)
    const authHeader = authToken
        ? {
            Authorization: `Bearer ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };
    

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
   
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }

    return ("/" + params)
}



function accountdetails(userid) {
    console.log("useridApi", endUrl.accountdetails + "?_id=" + userid)
    return callApi(endUrl.accountdetails + "?select=profile%2Fcontact%2Flocation", "GET", null, null,settingss.authorization);

}

function accountdetailsUpdate(Firstname, Lastname, Email, Language, Country, Gender, PhoneNo, DateofBirth, UserID) {

    const body = JSON.stringify({
        // product_id: '5adf5b82b97636000f503045',
        Firstname: Firstname,
        Lastname: Lastname,
        Email: Email,
        Language: Language,
        Country: Country,
        Gender: Gender,
        PhoneNo: PhoneNo,
        DateofBirth: DateofBirth,
        UserID: UserID,
     
    });

    return callApi(endUrl.accountdetailsUpdate +"?_id=" + UserID, "POST", body, null, settingss.authorization)
}


export default {
    accountdetails,
    accountdetailsUpdate
};