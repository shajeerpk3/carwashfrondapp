import React from "react";
import settingss from '../../constants/Settings';

const Url = {
    // Execution: "https://api.paypal.com/v1/payments/payment/",//live
    Execution: "https://api.sandbox.paypal.com/v1/payments/payment/",//sandbox
}



function callApi(endpoint, method, body, params, authToken, lastaddid = null) {

    let bodyIsJson = true;

    const authHeader = authToken
        ? {
            "Authorization": `Bearer ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/json",
        }
        : {};
    // console.log("endpoint", endpoint)
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: JSON.stringify({
            "payer_id": body
        })
    };


    return fetch(endpoint, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }
            return json;
        });

}

function Execution(token, payer_id, payment_Id) {

    const body = JSON.stringify({

        payer_id: payer_id

    });


    return callApi(Url.Execution + payment_Id + "/execute/", "POST", payer_id, null, token)

}


export default {
    Execution,
};