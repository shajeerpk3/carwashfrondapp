import settingss from '../../constants/Settings';

const servername = settingss.serverUrl3;
const endUrl = {
    Regist: servername + "/users/register",
    Starter:servername + "/users/authenticate",
   
}
function callApi(endpoint, method, body, params, authToken,lastaddid=null) {
   
    let bodyIsJson = false;
    // try {
    //     JSON.parse(body);
    //     // if(body==null)
    //     // {
    //     //     bodyIsJson = false;
    //     // }
    // } catch (e) {
    //     bodyIsJson = false;
    //   //  console.log("mindulaaa",bodyIsJson)
    // }

   // console.log("mindano")

    let fullUrl:string = endpoint + constructParams(params);

    // if(lastaddid!=null){
        fullUrl=fullUrl + DeleteParam(lastaddid);
    // }
  
// console.log("fullUrl",fullUrl)
    const authHeader = authToken
        ? {
            Authorization: `Bearer ${authToken}`,
        }
        : {};

    const contentTypeHeader = bodyIsJson
        ? {
            "Content-Type": "application/x-www-form-urlencoded",
        }
        : {};
    const fetchOptions = {
        method,
        headers: {
            ...authHeader,
            ...contentTypeHeader,
        },
        body: body,
    };
    //console.log("hello",fetchOptions)
    // if (type == "op") {
    //     console.log("fullUrl")
    //     return fetch(fullUrl, fetchOptions)
    //         .then((response) => console.log("rerererer",response))

    // }
    // if (type != "op") {

    return fetch(fullUrl, fetchOptions)
        .then((response) => response.json().then((json) => ({ json, response })))
        .then(({ json, response }) => {

            if (!response.ok) {
                return Promise.reject(json);
            }

            return json;
        });
    // }
}


function constructParams(params) {
    if (!params) {
        return "";
    }
    const paramFragments = [];
    Object.keys(params).forEach((param) => {
        paramFragments[paramFragments.length] = `${param}=${encodeURI(params[param])}`;
    });
    return "?" + paramFragments.join("&");
}

function DeleteParam(params) {
    if (!params) {
        return "";
    }
  
    return ("/"+ params)
}







function Regist(params) {

   

    return callApi(endUrl.Regist, "POST", null, params, null)
}

function Starter(email,password) {

    const body = JSON.stringify ({      
        email: email,     
        password:password,      
    });

    return callApi(endUrl.Starter + "?email=" +email+ "&password="+ password, "POST", body, null, null)
}



export default {
    Regist,
    Starter
    
};













